from benchmarking.tools.write import write
from benchmarking.tools.read import read
from benchmarking.tools.tools import output
from benchmarking.tools.database import reset_db, create_db, get_db_size, get_db_rows
from benchmarking.settings import Settings
from benchmarking.tools.configs import Configs
from datetime import datetime

from uuid import uuid4
import cProfile, io, pstats, logging, os, time, psutil


def main():
    base_amount = len(Settings.database) * len(Settings.payloads) * len(Settings.results) * len(Settings.decode)
    write_amount = base_amount * len(Settings.scans) * len(Settings.write_access)
    read_amount = base_amount * len(Settings.read_access)

    write_count = 1
    read_count = 1
    cur_time = datetime.now().strftime("%H:%M:%S")

    for payloads in Settings.payloads:
        with open(os.path.join(output.base_path, "write.txt"), "a") as f:
            f.write(f"payloads: {payloads}\n")
        with open(os.path.join(output.base_path, "read.txt"), "a") as f:
            f.write(f"payloads: {payloads}\n")

        for database in Settings.database:
            db_url = Settings.db_url.format(DATABASE=database[0])
            backend = database[1]
            for results in Settings.results:
                for decode in Settings.decode:
                    # Writing
                    for access in Settings.write_access:
                        for scans in Settings.scans:
                            uuid = uuid4().hex

                            output.configure_logger(os.path.join(payloads, access), f"write_{uuid}.txt")

                            benchmark_name = f"{cur_time}_{payloads}_{access}_{uuid}"

                            logging.info(
                                f"_____________________________________________________________________write: {write_count}/{write_amount}"
                            )
                            logging.info(f"benchmark: {benchmark_name}")
                            # logging.info(f"CPU: {psutil.Process(os.getpid()).cpu_num()}")

                            write_count += 1

                            logging.benchmark(f"database: {database[0]}, type: {backend.name}")
                            logging.benchmark(f"payloads: {payloads}, results: {results}, access: {access}")
                            logging.benchmark(f"scans: {scans}")
                            # logging.benchmark(f"db url: {db_url}")
                            logging.benchmark(
                                f"runkey: {Settings.felix} felix, {Settings.optoboard} optoboard, {Settings.frontend} frontend"
                            )
                            logging.benchmark(f"time: {datetime.now().strftime('%d.%m.%Y, %H:%M:%S')}")

                            if Settings.profile:
                                profiler = cProfile.Profile()
                                profiler.enable()

                            for i in range(Settings.write_repeats):
                                logging.benchmark(f"_____________________________________________________________________")
                                logging.info(
                                    f"_____________________________________________________________________repeat: {i+1}/{Settings.write_repeats}"
                                )

                                if Settings.reset:
                                    reset_db(db_url, backend)
                                else:
                                    create_db(db_url, backend)
                                output.configure_logger(os.path.join(payloads, access), f"write_{uuid}.txt")

                                # create_runkey_dif(db, ids["fe_ids"], 0.1, frontend, optoboard, felix)
                                scan_amount = len(scans)
                                scan_count = 1
                                for scan in scans:
                                    runkey_name = f"rk_{scan}_{uuid4().hex}"
                                    logging.info(
                                        f"_____________________________________________________________________scan: {scan_count}/{scan_amount}"
                                    )
                                    logging.benchmark(f"_____________________________________________________________________")

                                    logging.benchmark(f"scan : {scan}, runkey: {runkey_name}")
                                    configs = Configs(scan, payloads, results)
                                    scan_count += 1

                                    logging.benchmark(
                                        f"felix payload size : {configs.get_felix_size()}, opto payload size : {configs.get_opto_size()}, frontend payload size : {configs.get_fe_size()}"
                                    )
                                    logging.benchmark("_____________________________________________________________________")

                                    start = time.perf_counter()

                                    identifier = write(
                                        db_url=db_url,
                                        backend=backend,
                                        runkey_name=runkey_name,
                                        access=access,
                                        configs=configs,
                                        json=not payloads.isdigit(),
                                    )

                                    finish = time.perf_counter()
                                    write_time = round(finish - start, 3)
                                    size = get_db_size(db_url, backend)

                                    logging.info("_____________________________________________________________________")
                                    logging.info(f"Full runkey insertion took {write_time} seconds.")
                                    logging.info(f"Size of runkey object is {size} MB.")

                                    logging.benchmark("_____________________________________________________________________")
                                    logging.benchmark(f"Runkey insertion: {write_time}")
                                    logging.benchmark(f"size of the database: {size} MB")
                                    logging.benchmark(f"rows in tables: {get_db_rows(db_url, backend)}")

                                    with open(os.path.join(output.base_path, "write.txt"), "a") as f:
                                        f.write(f"{write_time} {size}\n")

                            if Settings.profile:
                                profiler.disable()
                                # profiler.dump_stats("/itk-demo-sw/configdb_benchmarks/output/mariadb_bulk_read.prof")
                                stream = io.StringIO()
                                stats = pstats.Stats(profiler, stream=stream).sort_stats("tottime")
                                stats.strip_dirs()
                                # stats.print_stats()
                                stats.dump_stats("/itk-demo-sw/configdb_benchmarks/output/export")

                    # Reading
                    for access in Settings.read_access:
                        if access == "full":
                            identifier = [runkey_name]

                        uuid = uuid4().hex

                        output.configure_logger(os.path.join(payloads, access), f"read_{uuid}.txt")

                        benchmark_name = f"{cur_time}_{payloads}_{access}_{uuid}"

                        logging.info(
                            f"_____________________________________________________________________read: {read_count}/{read_amount}"
                        )
                        logging.info(f"benchmark: {benchmark_name}")

                        read_count += 1

                        logging.benchmark(f"database: {database[0]}, type: {backend.name}")
                        logging.benchmark(f"payloads: {payloads}, results: {results}, access: {access}, decode = {decode}")
                        # logging.benchmark(f"db url: {db_url}")
                        logging.benchmark(f"runkey: {Settings.felix} felix, {Settings.optoboard} optoboard, {Settings.frontend} frontend")
                        logging.benchmark(
                            f"felix payload size : {configs.get_felix_size()}, opto payload size : {configs.get_opto_size()}, frontend payload size : {configs.get_fe_size()}"
                        )

                        logging.benchmark(f"time: {datetime.now().strftime('%d.%m.%Y, %H:%M:%S')}")
                        logging.benchmark("_____________________________________________________________________")

                        for i in range(Settings.read_repeats):
                            logging.benchmark(f"_____________________________________________________________________")
                            logging.info(
                                f"_____________________________________________________________________repeat: {i+1}/{Settings.read_repeats}"
                            )
                            start = time.perf_counter()

                            size = read(db_url=db_url, backend=backend, identifier=identifier, access=access, decode=decode)

                            finish = time.perf_counter()
                            read_time = round(finish - start, 3)

                            logging.benchmark("_____________________________________________________________________")
                            logging.benchmark(f"Read runkey: {read_time}")
                            logging.benchmark(f"Runkey object size: {size} MB")

                            logging.info("_____________________________________________________________________")
                            logging.info(f"Read full runkey in {read_time} seconds.")
                            logging.info(f"Size of runkey object is {size} MB.")

                            with open(os.path.join(output.base_path, "read.txt"), "a") as f:
                                f.write(f"{read_time} {size}\n")


if __name__ == "__main__":
    main()
