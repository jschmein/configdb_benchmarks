import os
from settings import Settings
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import hashlib


base_path = os.path.join(Settings.base_path, "output")


def string_to_color(input_string):
    hash_object = hashlib.md5(input_string.encode())
    hex_dig = hash_object.hexdigest()
    return "#" + hex_dig[:6]


def create_plots(path, lst, file):
    payloads = []
    with open(os.path.join(path, "write.txt"), "r") as f:
        while True:
            line = f.readline()
            if not line:
                break
            if "payloads" in line:
                payloads.append(int(line.split(": ")[1]))

    fig_time, ax_time = plt.subplots()
    fig_size, ax_size = plt.subplots()
    datapoints = lst[0]["amount"]

    for dct in lst:
        dct["times"] = [[] for _ in range(datapoints)]
        dct["sizes"] = [[] for _ in range(datapoints)]

    # Parse the data
    with open(os.path.join(path, f"{file}.txt"), "r") as f:
        for j in range(len(payloads)):
            for dct in lst:
                for i in range(dct["amount"]):
                    line = f.readline()
                    if not line:
                        break
                    elif "payloads" in line:
                        line = f.readline()

                    time, size = line.split(" ")
                    dct["times"][i].append(float(time.strip()))
                    dct["sizes"][i].append(float(size.strip()))

    # Plot the data
    legend_elements = []
    added_benchmarks = set()
    for dct in lst:
        color = dct["color"]
        benchmark = dct["name"]
        for i in range(dct["amount"]):
            ax_time.scatter(payloads, dct["times"][i], color=color, label=benchmark)
            ax_size.scatter(payloads, dct["sizes"][i], color=color, label=benchmark)
            if benchmark not in added_benchmarks:
                legend_elements.append(Line2D([0], [0], marker="o", color="w", label=benchmark, markerfacecolor=color, markersize=10))
                added_benchmarks.add(benchmark)

    # Create a legend from custom artist/label lists
    ax_time.legend(handles=legend_elements)
    ax_size.legend(handles=legend_elements)

    ax_time.set_xlabel("Payload size (kb)")
    ax_time.set_ylabel("Time (s)")

    ax_size.set_xlabel("Payload size (kb)")
    ax_size.set_ylabel("Memory (MB)")

    fig_time.savefig(os.path.join(path, f"{file}_time.png"))
    # fig_size.savefig(os.path.join(path, f"{file}_size.png"))


if __name__ == "__main__":
    path = os.path.join(base_path, "2024-02-29", "postgres")
    lst = [
        {"name": "sub postgres", "amount": 5, "color": "orange"},
        {"name": "full postgres", "amount": 5, "color": "blue"},
        # {"name": "sub mariadb", "amount": 5, "color": "red"},
        # {"name": "full mariadb", "amount": 5, "color": "green"},
    ]
    create_plots(path, lst, "read")
    create_plots(path, lst, "write")
