import os
from settings import Settings
import matplotlib.pyplot as plt

base_path = os.path.join(Settings.base_path, "output")


def create_plots(path, count, dir):
    sub_time = []
    sub_size = []
    full_time = []
    full_size = []
    payloads = []

    source_path = os.path.join(path, count, dir)
    # Parse the data
    with open(os.path.join(source_path, "plot.txt"), "r") as f:
        while True:
            line = f.readline()
            if not line:
                break
            if "payloads" in line:
                # times = []
                # sizes = []
                payload, access = line.split(", ")
                payload = int(payload.split(": ")[1])
                access = access.split(": ")[1].strip()
            else:
                time, size = line.split(" ")
                # times.append(float(time.strip()))
                # sizes.append(float(size.strip()))
                time = float(time.strip())
                size = float(size.strip())
                if access == "sub":
                    payloads.append(payload)
                    sub_time.append(time)
                    sub_size.append(size)
                else:
                    full_time.append(time)
                    full_size.append(size)

    # Plot the data
    fig, ax = plt.subplots()

    ax.scatter(payloads, sub_time, label="sub access time")
    ax.scatter(payloads, full_time, label="full access time")
    ax.set_xlabel("Payload size (kb)")
    ax.set_ylabel("Time (s)")
    ax.legend()

    fig.savefig(os.path.join(source_path, "time.png"))

    fig, ax = plt.subplots()

    ax.scatter(payloads, sub_size, label="sub access memory")
    ax.scatter(payloads, full_size, label="full access memory")
    ax.set_xlabel("Payload size (kb)")
    ax.set_ylabel("Memory (MB)")
    ax.legend()

    fig.savefig(os.path.join(source_path, "size.png"))


if __name__ == "__main__":
    path = os.path.join(base_path, "2024-01-25")
    create_plots(path, "2", "write")
    create_plots(path, "2", "read")
