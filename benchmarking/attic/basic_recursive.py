from configdb_server.database_tools import Database
from benchmarking.tools.tools import getsize

from math import floor
import time
import logging


FELIX_FILE = "controller.json"
FE_FILE = "chip1_random.json"  # "fe_config_compr.json" #"0x15418_L2_1280_warm.json"
DIR = "/mnt/storage/home/schmeing/itk-demo-sw/configdb-server/config/"


def read_runkey_wo_payload(db: Database):
    start = time.perf_counter()
    tree = db.get_tag("rk1", False, verbosity=1)
    rk = time.perf_counter()
    logging.info(f"Read full runkey (w/o payload) in {round(rk-start, 3)} seconds.")

    felix_id = tree["objects"][0]["children"][0]["id"]
    felix_data = db.get_tree(felix_id, False, verbosity=3)
    felix = time.perf_counter()
    logging.info(f"Read felix data in {round(felix-rk, 3)} seconds.")


# @profile
def read_runkey(db: Database):
    start = time.perf_counter()
    rk = db.get_tag("benchmark_rk", False, verbosity=3)
    # logging.info(rk)
    rk_time = time.perf_counter()
    logging.info(f"Read runkey in {round(rk_time-start, 3)} seconds.")
    logging.info(f"Size of runkey object is {round(getsize(rk)/1024/1024, 4):,} Mb.")
    # logging.info(rk["objects"][0]["children"][111]["children"][3]["children"][10]["payloads"][0]["data"])


def create_runkey_stage(db: Database, frontend, optoboard, felix):
    felix = 200
    optoboard = 1500
    frontend = 34000

    felix_config = open(DIR + FELIX_FILE, "r").read()
    fe_config = open(DIR + FE_FILE, "r").read()

    root = db.create_root("root_tag")

    start = time.perf_counter()

    felix_ids = []
    i = 1
    while i <= felix:
        id = db.add_to_tree("felix", True, parents=[root], payloads=[{"data": felix_config, "type": "felix"}])
        felix_ids.append(id)
        i = i + 1

    felix_time = time.perf_counter()
    logging.info(f"Created {felix} felix objects and payloads in {round(felix_time-start, 3)} seconds.")

    opto_ids = []
    i = 1
    while i <= optoboard:
        parent = felix_ids[floor(felix * i / optoboard) - 1]
        id = db.add_to_tree("optoboard", True, parents=[parent], payloads=[{"data": felix_config, "type": "optoboard"}])
        opto_ids.append(id)
        i = i + 1

    opto_time = time.perf_counter()
    logging.info(f"Created {optoboard} optoboard objects and payloads in {round(opto_time-felix_time, 3)} seconds.")

    i = 1
    while i <= frontend:
        parent = opto_ids[floor(optoboard * i / frontend) - 1]
        db.add_to_tree("frontend", True, parents=[parent], payloads=[{"data": fe_config, "type": "frontend"}])
        i = i + 1

    frontend_time = time.perf_counter()
    logging.info(f"Created {frontend} frontend objects and payloads in {round(frontend_time-opto_time, 3)} seconds.")
    logging.info(f"Complete runkey creation took {round(frontend_time-start, 3)} seconds.")


def commit_runkey(db: Database):
    start = time.perf_counter()
    db.commit_tree("root_tag", name="rk1", author="JS")
    end = time.perf_counter()
    logging.info(f"Committed full runkey in {round(end-start, 3)} seconds.")


def create_runkey(db: Database, frontend, optoboard, felix):
    felix_config = open(DIR + FELIX_FILE, "r").read()
    fe_config = open(DIR + FE_FILE, "r").read()

    root = db.backend.create_object("root")
    tag = db.backend.create_tag("benchmark_rk", "runkey", [root])

    start = time.perf_counter()

    felix_ids = []
    i = 1

    while i <= felix:
        payl = db.backend.create_payload("felix", felix_config)
        id = db.backend.create_object("felix", parents=[root], payloads=[payl])
        felix_ids.append(id)
        i = i + 1

    felix_time = time.perf_counter()
    logging.info(f"Created {felix} felix objects and payloads in {round(felix_time-start, 3)} seconds.")

    opto_ids = []
    i = 1
    while i <= optoboard:
        parent = felix_ids[floor(felix * i / optoboard) - 1]
        payl = db.backend.create_payload("optoboard", felix_config)
        id = db.backend.create_object("optoboard", parents=[parent], payloads=[payl])
        opto_ids.append(id)
        i = i + 1

    opto_time = time.perf_counter()
    logging.info(f"Created {optoboard} optoboard objects and payloads in {round(opto_time-felix_time, 3)} seconds.")

    fe_ids = []
    i = 1
    while i <= frontend:
        parent = opto_ids[floor(optoboard * i / frontend) - 1]
        payl = db.backend.create_payload("frontend", fe_config)
        id = db.backend.create_object("frontend", parents=[parent], payloads=[payl])
        fe_ids.append(payl)
        i = i + 1

    frontend_time = time.perf_counter()
    logging.info(f"Created {frontend} frontend objects and payloads in {round(frontend_time-opto_time, 3)} seconds.")
    logging.info(f"Complete runkey creation took {round(frontend_time-start, 3)} seconds.")

    return root, fe_ids


def create_runkey_dif(db: Database, fe_ids, dif, frontend, optoboard, felix):
    dif_fe = int(frontend * dif)

    felix_config = open(DIR + FELIX_FILE, "r").read()
    fe_config = open(DIR + FE_FILE, "r").read()

    root = db.backend.create_object("root")
    # tag = db.backend.create_tag("benchmark_dif_rk", "runkey", [root])

    start = time.perf_counter()

    felix_ids = []
    i = 1
    while i <= felix:
        payl = db.backend.create_payload("felix", felix_config)
        id = db.backend.create_object("felix", parents=[root], payloads=[payl])
        felix_ids.append(id)
        i = i + 1

    felix_time = time.perf_counter()
    logging.info(f"Created {felix} felix objects and payloads in {round(felix_time-start, 3)} seconds.")

    opto_ids = []
    i = 1
    while i <= optoboard:
        parent = felix_ids[floor(felix * i / optoboard) - 1]
        payl = db.backend.create_payload("optoboard", felix_config)
        id = db.backend.create_object("optoboard", parents=[parent], payloads=[payl])
        opto_ids.append(id)
        i = i + 1

    opto_time = time.perf_counter()
    logging.info(f"Created {optoboard} optoboard objects and payloads in {round(opto_time-felix_time, 3)} seconds.")

    i = 1
    while i <= frontend:
        parent = opto_ids[floor(optoboard * i / frontend) - 1]
        if i <= dif_fe:
            payl = db.backend.create_payload("frontend", fe_config)
            id = db.backend.create_object("frontend", parents=[parent], payloads=[payl])
        else:
            id = db.backend.create_object("frontend", parents=[parent], payloads=[fe_ids[i - dif_fe - 1]])
        i = i + 1

    frontend_time = time.perf_counter()
    logging.info(f"Created {frontend} frontend objects and payloads in {round(frontend_time-opto_time, 3)} seconds.")
    logging.info(f"Complete runkey creation took {frontend_time-start} seconds.")

    return root
