from configdb_server.database_tools import Database, Backends
from benchmarking.model.basic_recursive import create_runkey, read_runkey

PORT = 32818


def main():
    db = Database(Backends.SQLALCHEMY_ORACLE, "", "sqlite:///stage.db")

    felix = 1
    optoboard = 1
    frontend = 1

    # db.backend.create_database()
    # db.backend.upgrade_database()

    root, fes = create_runkey(db, frontend, optoboard, felix)
    # create_runkey_dif(db, fes, 0.1, frontend, optoboard, felix)

    read_runkey(db)


if __name__ == "__main__":
    main()
