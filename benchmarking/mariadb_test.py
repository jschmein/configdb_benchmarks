import mysql.connector
from benchmarking.tools.listGen import ListGenerator
from benchmarking.settings import Settings
from benchmarking.tools.configs import Configs
from benchmarking.tools.database import reset_db, create_db

from configdb_server.database_tools import Backends

from multiprocessing import Pool
import logging, time, psutil, os

parallel = False
payload = "80"
felix = 1
optoboard = 1500
frontend = 35000


def write():
    logging.basicConfig(level=logging.INFO)

    db_url = Settings.db_url.format(DATABASE=Settings.database[0][0])
    reset_db(db_url, Backends.MARIADB)
    create_db(db_url, Backends.MARIADB)

    configs = Configs(0, payload, "none")

    start_time = time.perf_counter()
    logging.info(f"Writing runkey:")
    if parallel:
        with Pool(processes=Settings.parallel_processes) as pool:
            args_list = [(i, configs) for i in range(felix)]
            ids = pool.starmap(write_sub_runkey, args_list)
    else:
        ids = []
        for i in range(felix):
            ids.append(write_sub_runkey(i, configs))

    finish_time = time.perf_counter()
    logging.info(f"Write time: {round(finish_time - start_time, 3)}")


def write_sub_runkey(i, configs: Configs):
    cnx = mysql.connector.connect(user="configdb", password="test", host="localhost", database="configdb")

    lists = ListGenerator(configs)
    lists.create_runkey_lists(round(frontend / felix), round(optoboard / felix), 1)

    query = "INSERT INTO payload (type, data, id) VALUES (%(type)s, %(data)s, %(id)s)"

    cursor = cnx.cursor()
    logging.info(f"Subrunkey {i}:")
    cursor.executemany(query, lists.payload_list)

    cnx.commit()
    cursor.close()
    cnx.close()

    felix_id = lists.felix_ids[0]
    lists.empty_lists()
    return felix_id


if __name__ == "__main__":
    write()
