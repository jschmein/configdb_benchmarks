Measure: memory, disk i/o, network i/o, cpu usage

Runkey: 35.000 frontends, 1.500 optoboards, 200 FELIX
    One config file per component
    7 scans per tuning with one result per frontend ?
    New configs for each scan -> new runkey for each scan?

Database: Mariadb in docker container on daqdev8 (https://wiki.uni-wuppertal.de/!detlab/doku.php?id=labnetwork)
    Access from local machine
    Network access (10GbE 100GbE)
    wupp-charon
    could run a test with postgres as well

Payload scenarios:
    Payloads saved in SQL
    Payloads saved in NoSQL
    Compressed Payloads
    Uncompressed payloads

Access patterns:
    Write complete runkey at once
    Write split up into sub-runkeys (per FELIX), allows for parallelization, because sub-runkey are independent
    Read complete runkey at once
    Read split up into sub-runkeys (per FELIX)
    
To test:
    Size of runkey
    Distribution of size
    Amount of closure table entries
    Read speed (differnt access patterns)
    Write speed (differnt access patterns)

Topologien:
    Single Server
    Cluster (varying size)
    Locale caches pro FELIX read

With ConfigDB
Range of payload size

Comparison of config validation:
pydantic vs https://github.com/jcrist/msgspec