import json, os

DIR = "tuning"
FILE_DIR = os.path.join(os.path.dirname(__file__), f"scans/{DIR}")


def name():
    with open(os.path.join(FILE_DIR, "attachments_info.json"), "r") as file:
        data = json.load(file)

    for dic in data:
        os.rename(
            os.path.join(FILE_DIR, dic["data_id"]), os.path.join(FILE_DIR, dic["title"])
        )


if __name__ == "__main__":
    name()
