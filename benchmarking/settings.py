import os
from configdb_server.database_tools import Backends


class Settings:
    felix = 200
    optoboard = 1500
    frontend = 35000
    # db_url = "mariadb+pymysql://configdb:test@{DATABASE}/configdb"
    db_url = "configdb:test@{DATABASE}/configdb"
    stage_url = "sqlite:///file:stage.db?mode=memory&uri=true"
    # description = "normal runkey, mariadb on daqdev8, local access"

    parallel_processes = 8
    parallel = True
    base_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    felix_file = "felix_config.json"
    opto_file = "opto_config.json"
    fe_config_path = os.path.join(base_path, "benchmarking/configs/tuning/data/")
    config_path = os.path.join(base_path, "benchmarking/configs/")
    write_repeats = 50
    read_repeats = 50
    profile = False
    reset = True

    database = [
        ("daqdev8-hs.detlab.lan:5432", Backends.SQLALCHEMY_POSTGRES),
        ("daqdev8-hs.detlab.lan:3306", Backends.SQLALCHEMY_MARIADB),
    ]
    # database = [
    #     ("daqdev8-hs.detlab.lan:3306", Backends.SQLALCHEMY_MARIADB),
    # ]
    # payloads = ["0", "80", "120", "200", "500"]  # frontend payload in kb, default qc_config file size is 120 kb
    payloads = ["0", "80", "120", "200"]
    payloads = ["200", "fe"]
    scans = [
        [
            "std_tune_pixelthreshold",
            "std_retune_pixelthreshold",
            "std_tune_globalthreshold",
            "std_retune_globalthreshold",
            "std_totscan",
            "std_thresholdscan_hd",
            "std_thresholdscan_hr",
        ],
        ["std_tune_pixelthreshold"],
    ]
    scans = [["std_thresholdscan_hr"]]
    results = ["none", "one", "all"]
    results = ["none"]

    write_access = ["sub", "full"]
    # write_access = ["sub"]

    read_access = ["sub", "full"]
    # read_access = ["full"]

    encode = [False]
    decode = [False]

    # benchmarks = {
    #     "1": {},
    #     "2": {},
    #     "3": {},
    #     "4": {},
    #     "5": {},
    #     "6": {},
    #     "7": {},
    #     "8": {},
    #     "9": {},
    # }


Settings = Settings()
