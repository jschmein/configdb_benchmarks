from numpy import random
from configdb_server.tools import compressStringToBytes
from configdb_server.logging import configure_logger as configure_logging
from benchmarking.settings import Settings
import os
import sys
from types import ModuleType, FunctionType
from gc import get_referents
from datetime import datetime


class Output:
    counter = 1

    def __init__(self):
        exists = True
        while exists:
            path = os.path.join(Settings.base_path, "output", datetime.now().strftime("%Y-%m-%d"), str(self.counter))
            if os.path.isdir(path):
                self.counter = self.counter + 1
            else:
                exists = False
                os.makedirs(path, exist_ok=True)
                self.base_path = path

    def create_dir(self, dir_name):
        path = os.path.join(self.base_path, dir_name)
        os.makedirs(path, exist_ok=True)
        self.path = path
        return path

    def configure_logger(self, dir_name, filename):
        self.create_dir(dir_name)
        file = os.path.join(self.path, filename)
        configure_logging(file)


output = Output()


def create_compressed_json_file():
    # Use the function to create a compressed JSON file
    path = "/home/arbeit/itk-demo-sw/configdb_benchmarks/benchmarking/configs/tuning/data/payloads"
    os.makedirs(path, exist_ok=True)
    for payload in Settings.payloads:
        try:
            size = int(payload) * 1024

            data = bytes(size)

            # Open the file in write mode
            with open(os.path.join(path, f"{payload}.bin"), "wb") as f:
                # Write the bytes object to the file
                f.write(data)
        except ValueError:
            pass


def replace_zeros(filename, filename2):
    with open(filename, "r") as file:
        content = file.read()

    modified_content = ""

    for c in content:
        if c == "0":
            modified_content = modified_content + str(random.randint(0, 2))
        else:
            modified_content = modified_content + c

    with open(filename2, "w") as file:
        file.write(modified_content)


def compress(filename, filename2):
    with open(filename, "r") as file:
        content = file.read()

    modified_content = compressStringToBytes(content)

    with open(filename2, "w") as file:
        file.write(str(modified_content))


# Custom objects know their class.
# Function objects seem to know way too much, including modules.
# Exclude modules as well.
BLACKLIST = type, ModuleType, FunctionType


def getsize(obj):
    """sum size of object & members."""
    if isinstance(obj, BLACKLIST):
        raise TypeError("getsize() does not take argument of type: " + str(type(obj)))
    seen_ids = set()
    size = 0
    objects = [obj]
    while objects:
        need_referents = []
        for obj in objects:
            if not isinstance(obj, BLACKLIST) and id(obj) not in seen_ids:
                seen_ids.add(id(obj))
                size += sys.getsizeof(obj)
                need_referents.append(obj)
        objects = get_referents(*need_referents)
    return size


if __name__ == "__main__":
    replace_zeros("benchmarking/configs/module_qc/fe1.json", "benchmarking/configs/module_qc/fe1_rand.json")

    compress("benchmarking/configs/module_qc/fe1.json", "benchmarking/configs/module_qc/fe1_compr.txt")
    compress("benchmarking/configs/module_qc/fe1_rand.json", "benchmarking/configs/module_qc/fe1_rand_compr.txt")
