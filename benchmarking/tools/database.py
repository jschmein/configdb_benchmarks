from sqlalchemy_utils import drop_database
from sqlalchemy.exc import OperationalError
from sqlalchemy import inspect, func, Table, MetaData, text
from configdb_server.database_tools import Database, Backends
from benchmarking.settings import Settings

import os, pymysql


def create_db(db_url, backend):
    db = Database(backend, db_url)
    db.backend.create_database()
    db.backend.upgrade_database()
    del db


def reset_db(db_url, backend):
    db = Database(backend, db_url)
    try:
        drop_database(db.url)
    except OperationalError:
        pass
    db.backend.create_database()
    db.backend.upgrade_database()
    del db

    # db.backend.create_database()
    # db.backend.upgrade_database()
    # db.backend.clear_database()


def get_db_filesize(backend, db_url=None):
    path = os.path.join(Settings.base_path, "docker")  # os.getenv("DOCKER_DIR")
    if backend == Backends.SQLALCHEMY_POSTGRES:
        db = Database(backend, db_url)

        # Create a Session
        session = db.backend.create_read_session()

        # Create a SQL query to get the size of the database
        query = text(f"SELECT oid FROM pg_database WHERE datname = 'configdb'")

        # Execute the query and get the result
        result = session.execute(query).scalar()

        # Close the connection
        session.close()

        dir_path = os.path.join(path, f"benchmark/postgres/base/{result}/")
    else:
        dir_path = os.path.join(path, "benchmark/mariadb/databases/configdb")

    # Get the total size of the directory
    total_size = 0
    for dirpath, dirnames, filenames in os.walk(dir_path):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            # skip if it is symbolic link
            if not os.path.islink(fp):
                total_size += os.path.getsize(fp)

    # print("Directory size: " + str(total_size / (1024 * 1024)) + " MB")
    return round(total_size / 1024 / 1024, 3)


def get_db_size(db_url, backend):
    db = Database(backend, db_url)

    # Create a Session
    session = db.backend.create_read_session()

    # Create a SQL query to get the size of the database
    if backend == Backends.SQLALCHEMY_POSTGRES:
        query = text(f"SELECT ROUND(pg_database_size('configdb') / 1024 / 1024) AS size_in_mb")
    else:
        query = text(
            f"SELECT ROUND(SUM(data_length + index_length) / 1024 / 1024, 2) "
            f"AS 'Size (MB)' "
            f"FROM information_schema.tables "
            f"WHERE table_schema = 'configdb'"
        )

    # Execute the query and get the result
    result = session.execute(query).scalar()

    # Close the connection
    session.close()

    return result


def get_db_rows(db_url, backend):
    # Create a connection to the database
    db = Database(backend, db_url)

    # Create a Session
    session = db.backend.create_read_session()

    # Create an inspector
    inspector = inspect(db.backend.owner_engine)
    metadata = MetaData()

    # Get the list of tables in the database
    table_names = inspector.get_table_names()

    dic = {}
    # For each table, get the number of rows
    for table_name in table_names:
        # Reflect the table
        table = Table(table_name, metadata, autoload_with=db.backend.owner_engine)
        count = session.query(func.count()).select_from(table).scalar()
        dic[table_name] = count

    # Close the Session
    session.close()

    return dic


def get_db_rows_old(host, port):
    # Create a connection to the database
    cnx = pymysql.connect(user="configdb", password="test", host=host, port=port, database="configdb")

    # Create a cursor object
    cursor = cnx.cursor()

    # Get the list of tables in the database
    cursor.execute("SHOW TABLES")
    tables = cursor.fetchall()

    dic = {}
    # For each table, get the number of rows
    for (table_name,) in tables:
        cursor.execute(f"SELECT COUNT(*) FROM `{table_name}`")
        number_of_rows = cursor.fetchone()[0]
        # print(f"Table: {table_name}, Number of rows: {number_of_rows}")
        dic[table_name] = number_of_rows

    # Close the cursor and connection
    cursor.close()
    cnx.close()

    return dic


if __name__ == "__main__":
    # get_db_rows("localhost", 3306)
    print(get_db_size("configdb:test@daqdev8-hs.detlab.lan:5432/configdb", Backends.SQLALCHEMY_POSTGRES))
    print(get_db_size("configdb:test@daqdev8-hs.detlab.lan:3306/configdb", Backends.SQLALCHEMY_MARIADB))
    print(get_db_filesize(Backends.SQLALCHEMY_POSTGRES, "configdb:test@daqdev8-hs.detlab.lan:5432/configdb"))
    print(get_db_filesize(Backends.SQLALCHEMY_MARIADB))
