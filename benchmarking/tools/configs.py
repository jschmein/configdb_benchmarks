from configdb_server.tools import compressStringToBytes
from benchmarking.settings import Settings

from uuid import uuid4
import os, math


class Configs:
    fe_configs = []
    felix_configs = []
    opto_configs = []

    def __init__(self, scan, payload, results) -> None:
        if payload == "0":
            fe_config = compressStringToBytes(uuid4().hex)
        elif payload.isdigit():
            fe_config = open(
                os.path.join(Settings.fe_config_path, "payloads", f"{payload}.bin"),
                "rb",
            ).read()
        else:
            fe_config = open(
                os.path.join(Settings.fe_config_path, "payloads", f"{payload}.json"),
                "r",
            ).read()

        felix_config = compressStringToBytes(open(os.path.join(Settings.config_path, Settings.felix_file), "r").read())
        opto_config = compressStringToBytes(open(os.path.join(Settings.config_path, Settings.opto_file), "r").read())

        fe_configs = [fe_config]

        if results != "none":
            result_path = os.path.join(Settings.fe_config_path, scan, "results", results)
            result_files = os.listdir(result_path)
            for result in result_files:
                with open(os.path.join(result_path, result), "r") as f:
                    content = compressStringToBytes(f.read())
                fe_configs.append(content)

        self.fe_configs = fe_configs
        self.felix_configs = [felix_config]
        self.opto_configs = [opto_config]

    def get_fe_size(self):
        size = 0
        for config in self.fe_configs:
            size += len(config)

        return math.ceil(size / 1024)

    def get_felix_size(self):
        size = 0
        for config in self.felix_configs:
            size += len(config)

        return math.ceil(size / 1024)

    def get_opto_size(self):
        size = 0
        for config in self.opto_configs:
            size += len(config)

        return math.ceil(size / 1024)
