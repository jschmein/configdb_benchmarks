from benchmarking.tools.configs import Configs
from uuid import uuid4


class ListGenerator:
    def __init__(
        self,
        configs: Configs,
        root_id=None,
    ):
        self.object_list = []
        self.payload_list = []
        self.object_payload_list = []
        self.metadata_list = []
        self.object_metadata_list = []
        self.closure_list = []
        self.fe_ids = []
        self.opto_ids = []
        self.felix_ids = []
        self.parents = {}

        self.fe_configs = configs.fe_configs
        self.opto_configs = configs.opto_configs
        self.felix_configs = configs.felix_configs
        if root_id is None:
            self.root_id = uuid4().hex
            self.object_list.append({"type": "root", "id": self.root_id})
            self.closure_list.append({"descendant_id": self.root_id, "ancestor_id": self.root_id, "depth": 0})
        else:
            self.root_id = root_id

    def empty_lists(self):
        self.object_list = []
        self.payload_list = []
        self.object_payload_list = []
        self.metadata_list = []
        self.object_metadata_list = []
        self.closure_list = []

    def create_runkey_lists(self, fe_amount, opto_amount, felix_amount, json: bool = False):
        self.felix_ids = self._add_datasets(felix_amount, self.felix_configs, "felix", [self.root_id], 1)
        self.opto_ids = self._add_datasets(opto_amount, self.opto_configs, "optoboard", self.felix_ids, 2)
        self.fe_ids.extend(self._add_datasets(fe_amount, self.fe_configs, "frontend", self.opto_ids, 3, json))

    def _add_datasets(self, amount, payloads, type, parent_ids, depth, json: bool = False):
        dataset_ids = []

        for i in range(0, amount):
            dataset_id = uuid4().hex
            self.object_list.append({"type": type, "id": dataset_id})

            if json:
                for payload in payloads:
                    payload_id = uuid4().hex
                    self.metadata_list.append({"type": f"{type}_config", "data": payload, "id": payload_id})
                    self.object_metadata_list.append({"object_id": dataset_id, "metadata_id": payload_id})
            else:
                for payload in payloads:
                    payload_id = uuid4().hex
                    self.payload_list.append({"type": f"{type}_config", "data": payload, "id": payload_id})
                    self.object_payload_list.append({"object_id": dataset_id, "payload_id": payload_id})

            # if metadata:
            #     metadata_id = uuid4().hex
            #     self.metadata_list.append({"type": f"{type}_config", "data": metadata, "id": metadata_id})
            #     self.object_metadata_list.append({"object_id": dataset_id, "metadata_id": metadata_id})

            self.parents[dataset_id] = parent_ids[i % len(parent_ids)]
            parent = dataset_id
            for j in range(1, depth + 1):
                parent = self.parents[parent]
                self.closure_list.append({"descendant_id": dataset_id, "ancestor_id": parent, "depth": j})

            self.closure_list.append({"descendant_id": dataset_id, "ancestor_id": dataset_id, "depth": 0})

            dataset_ids.append(dataset_id)
        return dataset_ids
