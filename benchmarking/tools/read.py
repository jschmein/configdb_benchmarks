from configdb_server.database_tools import Database, Backends
from benchmarking.tools.tools import getsize, output
from benchmarking.settings import Settings
from multiprocessing import Pool
import logging, time, os


def read(db_url, backend, identifier, access, decode):
    logging.info(f"Reading runkey:")

    if access == "sub":
        size = 0
        if Settings.parallel:
            with Pool(processes=Settings.parallel_processes) as pool:
                args_list = [(i, db_url, backend, identifier[i], decode) for i in range(Settings.felix)]
                sizes = pool.starmap(read_sub_runkey, args_list)
            size = sum(sizes)
        else:
            for i in range(Settings.felix):
                size = size + read_sub_runkey(i, db_url, backend, identifier[i], decode=decode)
        size = round(size, 3)

    elif access == "full":
        db = Database(backend, db_url)
        if Settings.profile:
            db.backend.profile.enable_profiling()

        rk = db.read_tree(identifier[0], payload_data=True, decode=decode)
        size = round(getsize(rk) / 1024 / 1024, 3)

        if Settings.profile:
            db.backend.profile.write_to_file(os.path.join(output.path, "profile_read.txt"), "a")
            db.backend.profile.profile_query(6, "", os.path.join(output.path, "profile_read_query_6.txt"), "a")
            db.backend.profile.profile_query(6, "CPU", os.path.join(output.path, "profile_read_query_6_CPU.txt"), "a")
            db.backend.profile.profile_query(6, "BLOCK IO", os.path.join(output.path, "profile_read_query_6_IO.txt"), "a")
            db.backend.profile.profile_query(6, "MEMORY", os.path.join(output.path, "profile_read_query_6_mem.txt"), "a")
            db.backend.profile.disable_profiling()
        del db

    return size


def read_sub_runkey(i, db_url, backend, root_id, decode):
    db = Database(backend, db_url)
    if Settings.profile:
        db.backend.profile.enable_profiling()

    sub_rk_time = time.perf_counter()
    rk = db.read_tree(root_id, payload_data=True, decode=decode)
    sub_rk_read = time.perf_counter() - sub_rk_time
    size = round(getsize(rk) / 1024 / 1024, 3)
    logging.info(f"Read subrunkey {i} in {round(sub_rk_read, 3)} seconds.")
    logging.info(f"Size of subrunkey {i} is {size} MB.")
    logging.benchmark(f"Read subrunkey {i}: {round(sub_rk_read, 3)}")
    logging.benchmark(f"Subrunkey {i} object size: {size} MB")

    if Settings.profile:
        db.backend.profile.write_to_file(os.path.join(output.path, "profile_read.txt"), "a")
        db.backend.profile.disable_profiling()
    del db
    return size
