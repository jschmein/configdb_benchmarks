from configdb_server.database_tools import Database, Backends
from benchmarking.tools.listGen import ListGenerator
from benchmarking.settings import Settings
from benchmarking.tools.configs import Configs
from benchmarking.tools.tools import output

from multiprocessing import Pool
import logging, time, psutil, os


def write(db_url, backend, runkey_name, access, configs, json: bool = False):
    logging.info(f"Writing runkey:")
    db = Database(backend, db_url)
    root_id = db.create_root(runkey_name)
    del db

    if access == "sub":
        if Settings.parallel:
            with Pool(processes=Settings.parallel_processes) as pool:
                args_list = [(i, db_url, backend, configs, root_id, json) for i in range(Settings.felix)]
                felix_ids = pool.starmap(write_sub_runkey, args_list)
        else:
            felix_ids = []
            for i in range(Settings.felix):
                felix_ids.append(write_sub_runkey(i, db_url, backend, configs, root_id, json))

    elif access == "full":
        db = Database(backend, db_url)
        if Settings.profile:
            db.backend.profile.enable_profiling()

        lists = ListGenerator(configs, root_id=root_id)
        lists.create_runkey_lists(Settings.frontend, Settings.optoboard, Settings.felix, json)
        write_database(db, lists, json)
        felix_ids = lists.felix_ids

        lists.empty_lists()
        if Settings.profile:
            db.backend.profile.disable_profiling()
        del db

    return felix_ids

    # logging.info(f"Adding scans runkey:")
    # for dir in os.listdir(config_path + "scans/"):
    #     logging.info(f"Scan: {dir}")
    #     result = compressStringToBytes(
    #         open(config_path + "scans/" + dir + "/results/output.log", "r").read()
    #     )
    #     settings = compressStringToBytes(
    #         open(config_path + "scans/" + dir + "/settings.json", "r").read()
    #     )

    #     lists.add_scan_to_runkey(1, option.frontend, settings, result)
    #     create_runkey_bulk(db, lists)
    #     lists.empty_lists()


def write_sub_runkey(i, db_url, backend, configs: Configs, root_id, json: bool = False):
    db = Database(backend, db_url)
    if Settings.profile:
        db.backend.profile.enable_profiling()

    logging.info(f"Subrunkey {i}:")
    lists = ListGenerator(configs, root_id=root_id)
    lists.create_runkey_lists(round(Settings.frontend / Settings.felix), round(Settings.optoboard / Settings.felix), 1, json)
    write_database(db, lists, json)

    felix_id = lists.felix_ids[0]
    lists.empty_lists()
    if Settings.profile:
        db.backend.profile.disable_profiling()
    del db
    return felix_id


# @profile
def write_database(db: Database, lists: ListGenerator, json: bool = False):
    start = time.perf_counter()

    table = "payload"
    ass_table = "object_payload"
    payl_list = lists.payload_list
    ass_payl_list = lists.object_payload_list
    if json:
        table = "metadata"
        ass_table = "object_metadata"
        payl_list = lists.metadata_list
        ass_payl_list = lists.object_metadata_list

    write_session = db.backend.create_write_session()
    db.backend.insert(write_session, "object", lists.object_list)
    object_time = time.perf_counter()
    # logging.info(f"CPU: {psutil.Process(os.getpid()).cpu_num()}")
    logging.benchmark(f"Object insertion: {round(object_time-start, 3)}")
    logging.info(f"Object insertion took {round(object_time-start, 3)} seconds.")
    if Settings.profile:
        db.backend.profile.write_to_file(os.path.join(output.path, "profile_object_insertion.txt"), "a")
        db.backend.profile.clear_profiling()

    db.backend.insert(write_session, table, payl_list)
    payload_time = time.perf_counter()
    # logging.info(f"CPU: {psutil.Process(os.getpid()).cpu_num()}")
    logging.benchmark(f"{table} insertion: {round(payload_time-object_time, 3)}")
    logging.info(f"{table} insertion took {round(payload_time-object_time, 3)} seconds.")
    if Settings.profile:
        db.backend.profile.write_to_file(os.path.join(output.path, f"profile_{table}_insertion.txt"), "a")
        db.backend.profile.clear_profiling()

    db.backend.insert(write_session, ass_table, ass_payl_list)
    object_payload_time = time.perf_counter()
    # logging.info(f"CPU: {psutil.Process(os.getpid()).cpu_num()}")
    logging.benchmark(f"{ass_table} insertion: {round(object_payload_time-payload_time, 3)}")
    logging.info(f"{ass_table} insertion took {round(object_payload_time-payload_time, 3)} seconds.")
    if Settings.profile:
        db.backend.profile.write_to_file(os.path.join(output.path, f"profile_{ass_table}_insertion.txt"), "a")
        db.backend.profile.clear_profiling()

    db.backend.insert(write_session, "closure", lists.closure_list)
    closure_time = time.perf_counter()
    logging.benchmark(f"closure insertion: {round(closure_time-object_payload_time, 3)}")
    logging.info(f"closure insertion took {round(closure_time-object_payload_time, 3)} seconds.")
    if Settings.profile:
        db.backend.profile.write_to_file(os.path.join(output.path, "profile_closure_insertion.txt"), "a")
        db.backend.profile.clear_profiling()

    db_time = time.perf_counter()
    # logging.benchmark(f"Runkey bulk insertion: {round(db_time-list_time, 3)}")
    logging.info(f"Runkey database bulk insertions took {round(db_time - start, 3)} seconds.")

    # connection = write_session.connection()
    # result = connection.execute(text("SELECT CONNECTION_ID()"))
    # connection_id = result.scalar()
    # logging.info(f"Connection id: {connection_id}")

    write_session.close()

    return lists.fe_ids
